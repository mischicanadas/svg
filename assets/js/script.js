$(document).ready(function(){
    var canvas = new fabric.Canvas('cv_flyer');

    canvas.setWidth($("#Div_Canvas").width()-30);
    
    function on_resize(c,t){onresize=function(){clearTimeout(t);t=setTimeout(c,100)};return c};
    on_resize(function() {        
        canvas.setWidth($("#Div_Canvas").width()-30);
    });

    var Add_Element_Type = 0;

    //Menu Active
    $(".lnk_menu").on("click",function(e){
      $("li").removeClass("active");
      $(this).closest("li").addClass("active");
    });

    canvas.on("object:selected", function(options) {
        options.target.bringToFront();
    });
    // select all objects
    function deleteObjects()
    {
        var activeObject = canvas.getActiveObject(),
        activeGroup = canvas.getActiveGroup();
        if (activeObject)
        {
            if (confirm('Are you sure you want to remove the element?'))
            {
                canvas.remove(activeObject);
            }
        }
        else if (activeGroup) {
            if (confirm('Are you sure you want to remove the element?'))
            {
                var objectsInGroup = activeGroup.getObjects();
                canvas.discardActiveGroup();
                objectsInGroup.forEach(function(object)
                {
                    canvas.remove(object);
                });
            }
        }
    }

    fabric.Image.fromURL('assets/img/realestate.svg', function(oImg) {
        canvas.add(oImg);
    });

    /*
    fabric.Image.fromURL('assets/img/background_houses.png', function(oImg) {
        oImg.left = 300;
        canvas.add(oImg);
    });
    */

    $("#btn_save").on("click",function(){

        var a = $("<a>").attr("href", canvas.toDataURL('png')).attr("download", "flyer.png").appendTo("body");
        a[0].click();
        a.remove();
    });

    $("#lnk_text").on("click",function(e){
        e.preventDefault();
        $("#Div_Message_Object").addClass("display-none");
        $("#Div_Message_Image").addClass("display-none");
        $("#Div_Message_Text").removeClass("display-none");

        $("#Modal_Message_Label").text("Canvas element");
        $('#Modal_Message_Body_Title').text('Text object');
        $('#Modal_Message').modal('show');
        Add_Element_Type = 1;
    });

    $("#lnk_object").on("click",function(e){
        e.preventDefault();
        $("#Div_Message_Object").removeClass("display-none");
        $("#Div_Message_Image").addClass("display-none");
        $("#Div_Message_Text").addClass("display-none");

        $("#Modal_Message_Label").text("Canvas element");
        $('#Modal_Message_Body_Title').text('Custom object');
        $('#Modal_Message').modal('show');
        Add_Element_Type = 2;
    });

    $("#lnk_image").on("click",function(e){
        e.preventDefault();
        $("#Div_Message_Object").addClass("display-none");
        $("#Div_Message_Image").removeClass("display-none");
        $("#Div_Message_Text").addClass("display-none");

        $("#Modal_Message_Label").text("Canvas element");
        $('#Modal_Message_Body_Title').text('Custom image');
        $('#Modal_Message').modal('show');
        Add_Element_Type = 3;
    });

    $("#lnk_delete").on("click",function(){
        deleteObjects();
    });

    $("#btn_image").on("click",function(){
        $("#btn_file").click();
    });

    $("#btn_file").on("change",function(e){
      var reader = new FileReader();
      reader.onload = function (event)
      {
        var imgObj = new Image();
        imgObj.src = event.target.result;
        imgObj.onload = function ()
        {
            var image = new fabric.Image(imgObj);
            image.set({
                left: 250,
                top: 250,
                angle: 0,
                padding: 10,
                cornersize: 10
            });

            canvas.add(image);
            $('#Modal_Message').modal('hide');
        }
    }
    reader.readAsDataURL(e.target.files[0]);

    });

    $("#btn_add_element").on("click",function(){
        if(Add_Element_Type == 1)
        {
            var _color = $("#txt_text_color").val();
            var add_text = new fabric.Text($("#txt_text").val(),{
                left: 100,
                top: 100,
                fill: _color
            });
            canvas.add(add_text);
        }

        if(Add_Element_Type == 2)
        {
          var _Element = $("#cmb_object").val();
          if(Number(_Element) == 1)
          {
            var _color = $("#cmb_object_color").val();
            var add_rectangle = new fabric.Rect({
                left: 100,
                top: 200,
                fill: _color,
                width: 300,
                height: 100
            });
            canvas.add(add_rectangle);
          }

          if(Number(_Element) == 2)
          {
            var _color = $("#cmb_object_color").val();
            var add_circle = new fabric.Circle({
                left: 100,
                top: 300,
                fill: _color,
                radius: 45
            });
            canvas.add(add_circle);
          }
        }
    });

    $("#btn_register").on("click",function(e){
        e.preventDefault();

        var _name = $("#txt_register_name").val();
        var _email = $("#txt_register_email").val();
        registerMail(_name,_email);
    });

    $("#btn_contact").on("click",function(e){
        e.preventDefault();

        var _name = $("#txt_contact_name").val();
        var _email = $("#txt_contact_email").val();
        registerMail(_name,_email);
    });

    function registerMail(_name, _email)
    {
        $("#Modal_Message_Label").text("Missing field");
        $("#Div_Message_Object").addClass("display-none");
        $("#Div_Message_Image").addClass("display-none");
        $("#Div_Message_Text").addClass("display-none");

        if(_name.length > 0)
        {
            if(_email.length > 0)
            {
                $.post("assets/php/Funciones.php",{Accion:"Notify_Me",Name:_name,Email:_email},function(data){
                    $('#Modal_Message_Body_Title').text(data);
                    $('#Modal_Message').modal('show');
                });
            }
            else
            {
                $('#Modal_Message_Body_Title').text('Email missing');
                $('#Modal_Message').modal('show');
            }
        }
        else
        {
            $('#Modal_Message_Body_Title').text('Name missing');
            $('#Modal_Message').modal('show');
        }
    }

});
