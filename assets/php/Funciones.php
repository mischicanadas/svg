<?php
ini_set('max_execution_time', 0);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if(isset($_POST['Accion']))
{
	$Resultado = "";
    
	$Accion = $_POST["Accion"];
	
    switch($Accion)
    {
        case "Notify_Me": $Resultado = Notify_Me();break;
    }
    
    echo $Resultado;
}

function Notify_Me()
{
    $Name = $_POST["Name"];
    $Email = $_POST["Email"];

    return "$Name with email $Email was added to the list!";
}

function Ejecuta_Query ($Query)
{
    include("ConnDB.php");
    
    //Abre una conexion a MySQL server
    $mysqli = new mysqli($ConnDB["Servidor"],$ConnDB["Usuario"],$ConnDB["Password"],$ConnDB["DB"]);

    //Arrojo cualquier error tipo connection error
    if ($mysqli->connect_error) {
         die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $result = $mysqli->query($Query);
    //Cierro la conexion 
    $mysqli->close();
    
    return $result;
}
?>